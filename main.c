#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>

#include "C.h"
#include "interface.h"

int main() {
  const int EXPECTED_VALUE = 2;
  int actual_value;
  int (*func)();
  void* handle;

  int int_from_a = get_int();
  printf("int from A: %d\n", int_from_a);

  /* Load library with dlopen */
  handle = dlopen("./libC.so", RTLD_NOW);
  if (!handle) {
    fprintf(stderr, "Error opening libC: %s\n", dlerror());
    exit(1);
  }

  func = dlsym(handle, "get_int_from_c");
  actual_value = func();

  if (actual_value != EXPECTED_VALUE) {
    fprintf(stderr, "dlopen: wrong symbol used.\n");
  } else {
    fprintf(stderr, "dlopen: correct symbol used.\n");
  }
  dlclose(handle);

  /* Load library with dlmopen */
  handle = dlmopen(LM_ID_NEWLM, "./libC.so", RTLD_NOW);
  if (!handle) {
    fprintf(stderr, "dlmopen error: %s\n", dlerror());
    exit(1);
  }

  func = dlsym(handle, "get_int_from_c");
  if (!func) {
    fprintf(stderr, "dlsym error: %s\n", dlerror());
    exit(1);
  }

  actual_value = func();
  if (actual_value != EXPECTED_VALUE) {
    fprintf(stderr, "dlmopen: wrong symbol used\n");
  } else {
    fprintf(stderr, "dlmopen: correct symbol used\n");
  }
  dlclose(handle);

  return 0;
}
